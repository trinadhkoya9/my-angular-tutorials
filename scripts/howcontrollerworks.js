		var app=angular.module('howcontrollerworksModule',[]);
		app.controller("howcontrollerworks",function($scope) {
			$scope.message="These are the List of Employees ";
		});


/**
 * [description] which return all the list of employees 
 * @param  {[type]} $scope [description]
 * @return {[type]}        [list data ]
 */
		app.controller('dataController',function($scope){

			var listEmployees=[
	    {
	        "firstname": "Trinadh",
	        "lastname": "Koya",
	        "gender": "Male",
	        "salary": 15000
	    },{
	        "firstname": "Steven",
	        "lastname": "Paul Jobs",
	        "gender": "Male",
	        "salary": 25000
	    }, {
	        "firstname": "Tim",
	        "lastname": "Cook",
	        "gender": "Male",
	        "salary": 35000
	    },{
	        "firstname": "Lisa",
	        "lastname": "Brendon",
	        "gender": "Male",
	        "salary": 40000
	    }, {
	        "firstname": "Reed",
	        "lastname": "Jobs",
	        "gender": "Male",
	        "salary": 45000
	    },{
	        "firstname": "Manoj",
	        "lastname": "Varma",
	        "gender": "Male",
	        "salary": 50000
	    }
	];

			$scope.employees=listEmployees;

		});
